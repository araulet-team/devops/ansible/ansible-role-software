# Ansible Role Software [![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

Role for installing software on Ubuntu.

## Requirements
This role requires Ansible 2.6 or higher, and platform requirements are listed in the metadata file.

## Installation

add this in **requirements.yml**

```shell
- src: git@gitlab.com:araulet-team/devops/ansible/ansible-role-software.git
  name: araulet.software
```

## Role Variables
The variables that can be passed to this role and a brief description about them are as follows:

```yaml
roles:
  - role: araulet.software
    become: true
    vars:
      chrome_install: true
      insomnia_install: true
      gitkraken_install: true
      slack_install: true
      slack_version: 4.4.2 # default version
    tags: [ 'software' ]
```

You can force software update with:

```yaml
chrome_force_update: true
slack_force_update: false
insomnia_force_update: false
gitkraken_force_update: false
```


## Dependencies

None

## Licence
MIT

## Author Information
Arnaud Raulet